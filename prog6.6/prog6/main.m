//
//  main.m
//  prog6
//
//  Created by Jim Ridge on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        /* int number, sign;
        
        NSLog(@"Please type in a number: ");
        scanf ("%i", &number);
        
        if ( number < 0 )
            sign = -1;
        else if ( number == 0 )
            sign = 0;
        else
            sign = 1;
        
        NSLog(@"Sign = %i", sign);
        
        */
        
        char c;
        
        NSLog(@"Enter a single character: ");
        scanf(" %c", &c);
        
        if (c >= 'a' && c <= 'z')
            NSLog(@"It's a lowercase alphabetic character.");
        else if  (c >= 'A' && c <= 'Z')
            NSLog(@"It's an uppercase alphabetic character.");
        else if ( c >= '0' && c <= '9')
            NSLog(@"It's a digit.");
        else
            NSLog(@"It's a special character.");
        
    }
    return 0;
}

