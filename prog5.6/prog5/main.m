//
//  main.m
//  prog5
//
//  Created by Jim Ridge on 31/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        unsigned int u, v, temp;
        
        NSLog(@"Please type in two nonnegative numbers.");
        scanf ("%u%u", &u, &v);
        
        while ( v != 0 ) {
            temp = u % v;
            u = v;
            v = temp;
        }
        
        NSLog(@"Their greatest common divisor is %u", u);
        
    }
    return 0;
}

