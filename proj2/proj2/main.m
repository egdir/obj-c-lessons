// Simple program to work with fractions - class version

#import <Foundation/Foundation.h>

//----- @interface section -----

@interface Fraction : NSObject 

-(void) print;
-(void) setNumerator: (int) n;
-(void) setDenominator: (int) d;
-(int) numerator;
-(int) denominator;

@end

//----- @implementation section -----

@implementation Fraction
{
    int numerator;
    int denominator;
}
-(void) print
{
    NSLog(@"%i/%i", numerator, denominator);
}
-(void) setDenominator: (int) d
{
    denominator = d;
}
-(void) setNumerator: (int) n
{
    numerator = n;
}
-(int) numerator
{
    return numerator;
}
-(int) denominator
{
    return denominator;
}


@end

//----- program section -----


int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        Fraction *frac1 = [Fraction new];
        Fraction *frac2 = [[Fraction alloc] init];
        // Create an instance of a fraction
        
        
        //Set fraction to 1/3
        
        [frac1 setNumerator: 1];
        [frac1 setDenominator: 3];
        
        //frac2
        
        [frac2 setNumerator: 12];
        [frac2 setDenominator: 113];
        
        // Display the fraction using the print method
        
        NSLog(@"The value of frac1 is:");
        [frac1 print];
        
        NSLog(@"And accessing the variables directly is %i/%i:", [frac1 numerator], [frac1 denominator]);
        
        NSLog(@"The value of frac2 is:");
        [frac2 print];
        
    }
    return 0;
}

