//
//  main.m
//  proj1
//
//  Created by Jim Ridge on 18/07/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        
        float sum;
        int i;
        //int myStringCount;
        
        NSMutableString* myString = [NSMutableString stringWithFormat:@"."];        
 
        sum = 50.6 + 25.3;
        
        NSLog(@"Programming is fun * %f!\nI mean really fun", sum);
        
        for (i=0; i<5; i++) {
            NSLog(@"%@%i", myString,i);
            
            [myString appendFormat:@"."];
            [myString insertString: @"test" atIndex:0];
        }
        
        //NSLog(@"One last time %@", myString);
        
        NSLog(@"Final count is %lu", [myString length]);
    }
    return 0;
}

