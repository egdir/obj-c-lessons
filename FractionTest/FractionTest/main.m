//
//  main.m
//  FractionTest
//
//  Created by Jim Ridge on 7/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Fraction.h"

int main (int argc, const char * argv[])
{

    @autoreleasepool {
        Fraction *myFraction = [[Fraction alloc] init];
        
        // set fraction to 1/3
        
        myFraction.numerator = 1; // [myFraction setNumerator: 1];
        myFraction.denominator = 3; // [myFraction setDenominator: 3];
        
        NSLog(@"The value of myFraction is:");
        [myFraction print];
        
    }
    return 0;
}

