//
//  Fraction.h
//  FractionTest
//
//  Created by Jim Ridge on 7/08/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// The Fraction class

@interface Fraction : NSObject

@property   int numerator, denominator;

-(void)     print;
/*
-(void)     setNumerator: (int) n;
-(void)     setDenominator: (int) d;
-(int)      numerator;
-(int)      denominator;
 */
-(double)   convertToNum;

@end
